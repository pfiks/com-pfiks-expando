/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.creator;

import java.util.Map;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.pfiks.expando.exception.ExpandoColumnCreationException;

/**
 * The Interface ExpandoColumnCreatorService.
 */
public interface ExpandoColumnCreatorService {

	/**
	 * It creates and configure the expando column based on the values passed.
	 *
	 * @param companyId the company to create the expando column in
	 * @param className the classname for the expando column
	 * @param fieldName the field name for the exapando column
	 * @param columnType the column type for the expando column, see the
	 *            constant
	 *            {@link com.liferay.expando.kernel.model.ExpandoColumnConstants}
	 * @param defaultValue the default value for the expando column
	 * @param columnSettings the settings for the expando column
	 * @param rolesPermissions the permissions to be granted to the roles as
	 *            individual scope. e.g: 'Guest',['VIEW','UPDATE']
	 * @return the expando column
	 * @throws ExpandoColumnCreationException if exceptions whilst creating the
	 *             new expando column.
	 */
	ExpandoColumn createMissingExpandoColumn(long companyId, String className, String fieldName, int columnType, Object defaultValue, String columnSettings, Map<String, String[]> rolesPermissions)
			throws ExpandoColumnCreationException;
}
