/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.creator;

import java.io.InputStream;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.model.Company;
import com.pfiks.expando.exception.ExpandoColumnCreationException;

/**
 * The Interface ExpandoColumnCreatorInputStreamService.
 */
public interface ExpandoColumnCreatorInputStreamService {

	/**
	 * Returns the ExpandoColumn if already present, otherwise will create and
	 * configure the expando column based on the values configured in the input
	 * stream
	 *
	 * @param company the company to create the expando column in
	 * @param expandoColumnClassName the classname for the expando column
	 * @param clazz the class to load the inputStream from
	 * @param inputStreamPath the path for the expando column configuration
	 *            input stream
	 * @return the expando column
	 * @throws ExpandoColumnCreationException if exceptions whilst creating the
	 *             new expando column or if an existing column is found but with
	 *             a type that does not match
	 *
	 */
	ExpandoColumn createExpandoColumn(Company company, String expandoColumnClassName, Class clazz, String inputStreamPath) throws ExpandoColumnCreationException;

	/**
	 * Returns the ExpandoColumn if already present, otherwise will create and
	 * configure the expando column based on the values configured in the input
	 * stream
	 *
	 * @param company the company to create the expando column in
	 * @param expandoColumnClassName the classname for the expando column
	 * @param inputStream the expando column configuration input stream
	 * @return the expando column
	 * @throws ExpandoColumnCreationException if exceptions whilst creating the
	 *             new expando column or if an existing column is found but with
	 *             a type that does not match
	 *
	 */
	ExpandoColumn createExpandoColumn(Company company, String expandoColumnClassName, InputStream inputStream) throws ExpandoColumnCreationException;
}
