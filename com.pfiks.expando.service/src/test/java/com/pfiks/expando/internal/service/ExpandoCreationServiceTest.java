/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.internal.service;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoTable;
import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.StreamUtil;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.pfiks.expando.internal.model.ExpandoField;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
@PrepareForTest(StreamUtil.class)
public class ExpandoCreationServiceTest extends PowerMockito {

	@InjectMocks
	private ExpandoCreationService expandoCreationService;

	@Mock
	private CreationUtil mockCreationUtil;

	@Mock
	private ExpandoColumnLocalService mockExpandoColumnLocalService;

	@Mock
	private ExpandoField mockExpandoField;

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Mock
	private ExpandoTable mockExpandoTable;

	@Before
	public void setUp() {

		mockStatic(StreamUtil.class);

	}

	@Test
	public void createExpandoField_WhenNoError_ThenReturnsExpandoFieldCreated() {

		String defaultValue = "value";
		String fieldName = "field";
		int columnType = 1;
		ExpandoField result = expandoCreationService.createExpandoField(fieldName, columnType, defaultValue);

		assertThat(result, notNullValue());
		assertThat(result.getName(), equalTo(fieldName));
		assertThat(result.getType(), equalTo(columnType));
		assertThat(result.getDefaultValue(), equalTo(defaultValue));
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void createMissingExpandoColumn_WhenException_ThenThrowsExpandoColumnCreationException() throws Exception {
		String className = "classNameValue";
		long companyId = 123;
		when(mockCreationUtil.getOrCreateDefaultTableForClass(companyId, className)).thenThrow(new PortalException());

		expandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField);
	}

	@Test
	public void createMissingExpandoColumn_WhenNoError_ThenReturnsTheCreatedExpandoColumn() throws Exception {
		String className = "classNameValue";
		long companyId = 123;
		long tableId = 456;
		int type = 55;
		Object defaultValue = "myDefaultValue";
		String fieldName = "fieldNameValue";
		when(mockCreationUtil.getOrCreateDefaultTableForClass(companyId, className)).thenReturn(mockExpandoTable);
		when(mockExpandoTable.getTableId()).thenReturn(tableId);
		when(mockExpandoField.getType()).thenReturn(type);
		when(mockExpandoField.getName()).thenReturn(fieldName);
		when(mockCreationUtil.getDefaultValue(mockExpandoField)).thenReturn(defaultValue);
		when(mockExpandoColumnLocalService.addColumn(tableId, fieldName, type, defaultValue)).thenReturn(mockExpandoColumn);
		when(mockCreationUtil.getRolePermissions(mockExpandoField)).thenReturn(Collections.emptyMap());

		ExpandoColumn result = expandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField);

		assertThat(result, sameInstance(mockExpandoColumn));
	}

	@Test
	@Parameters({ "", "null" })
	public void createMissingExpandoColumn_WhenColumnTypeSettingsAreNotSpecified_ThenDoesNotUpdateTheColumnTypeSettings(String typeSettings) throws Exception {
		String className = "classNameValue";
		long companyId = 123;
		long tableId = 456;
		int type = 55;
		Object defaultValue = "myDefaultValue";
		String fieldName = "fieldNameValue";
		when(mockCreationUtil.getOrCreateDefaultTableForClass(companyId, className)).thenReturn(mockExpandoTable);
		when(mockExpandoTable.getTableId()).thenReturn(tableId);
		when(mockExpandoField.getType()).thenReturn(type);
		when(mockExpandoField.getName()).thenReturn(fieldName);
		when(mockCreationUtil.getDefaultValue(mockExpandoField)).thenReturn(defaultValue);
		when(mockExpandoColumnLocalService.addColumn(tableId, fieldName, type, defaultValue)).thenReturn(mockExpandoColumn);
		when(mockCreationUtil.getRolePermissions(mockExpandoField)).thenReturn(Collections.emptyMap());
		when(mockCreationUtil.getColumnTypeSettings(mockExpandoField)).thenReturn(typeSettings);

		expandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField);

		verify(mockExpandoColumnLocalService, never()).updateTypeSettings(anyLong(), anyString());
	}

	@Test
	public void createMissingExpandoColumn_WhenColumnTypeSettingsAreSpecified_ThenUpdatesTheColumnTypeSettings() throws Exception {
		String className = "classNameValue";
		long companyId = 123;
		long tableId = 456;
		int type = 55;
		Object defaultValue = "myDefaultValue";
		String fieldName = "fieldNameValue";
		String typeSettings = "typeSettingsValue";
		when(mockCreationUtil.getOrCreateDefaultTableForClass(companyId, className)).thenReturn(mockExpandoTable);
		when(mockExpandoTable.getTableId()).thenReturn(tableId);
		when(mockExpandoField.getType()).thenReturn(type);
		when(mockExpandoField.getName()).thenReturn(fieldName);
		when(mockCreationUtil.getDefaultValue(mockExpandoField)).thenReturn(defaultValue);
		when(mockExpandoColumnLocalService.addColumn(tableId, fieldName, type, defaultValue)).thenReturn(mockExpandoColumn);
		when(mockCreationUtil.getRolePermissions(mockExpandoField)).thenReturn(Collections.emptyMap());
		when(mockCreationUtil.getColumnTypeSettings(mockExpandoField)).thenReturn(typeSettings);

		expandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField);

		verify(mockCreationUtil, times(1)).setColumnSettings(mockExpandoColumn, typeSettings);
	}

	@Test
	public void createMissingExpandoColumn_WhenNoPermissionsAreSpecified_ThenDoesNotConfigureColumnPermissions() throws Exception {
		String className = "classNameValue";
		long companyId = 123;
		long tableId = 456;
		int type = 55;
		Object defaultValue = "myDefaultValue";
		String fieldName = "fieldNameValue";
		when(mockCreationUtil.getOrCreateDefaultTableForClass(companyId, className)).thenReturn(mockExpandoTable);
		when(mockExpandoTable.getTableId()).thenReturn(tableId);
		when(mockExpandoField.getType()).thenReturn(type);
		when(mockExpandoField.getName()).thenReturn(fieldName);
		when(mockCreationUtil.getDefaultValue(mockExpandoField)).thenReturn(defaultValue);
		when(mockExpandoColumnLocalService.addColumn(tableId, fieldName, type, defaultValue)).thenReturn(mockExpandoColumn);
		when(mockCreationUtil.getRolePermissions(mockExpandoField)).thenReturn(Collections.emptyMap());

		expandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField);

		verify(mockCreationUtil, never()).configureColumnPermissionForRole(anyLong(), anyLong(), anyString(), any());
	}

	@Test
	public void createMissingExpandoColumn_WhenRolePermissionsAreSpecified_ThenConfiguresRolePermissions() throws Exception {
		String className = "classNameValue";
		long companyId = 123;
		long tableId = 456;
		int type = 55;
		Object defaultValue = "myDefaultValue";
		String fieldName = "fieldNameValue";
		when(mockCreationUtil.getOrCreateDefaultTableForClass(companyId, className)).thenReturn(mockExpandoTable);
		when(mockExpandoTable.getTableId()).thenReturn(tableId);
		when(mockExpandoField.getType()).thenReturn(type);
		when(mockExpandoField.getName()).thenReturn(fieldName);
		when(mockCreationUtil.getDefaultValue(mockExpandoField)).thenReturn(defaultValue);
		when(mockExpandoColumnLocalService.addColumn(tableId, fieldName, type, defaultValue)).thenReturn(mockExpandoColumn);
		Map<String, String[]> rolePermissions = new HashMap<>();
		rolePermissions.put("key1", new String[] { "val1", "val11" });
		rolePermissions.put("key2", new String[] { "val2", "val22" });
		when(mockCreationUtil.getRolePermissions(mockExpandoField)).thenReturn(rolePermissions);

		expandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField);

		verify(mockCreationUtil, times(1)).setColumnPermissions(mockExpandoColumn, rolePermissions);
	}

	@Test
	public void getExistingExpandoColumn_WhenExpandoColumnNotFound_ThenReturnsEmptyOptional() throws ExpandoColumnCreationException {
		String className = "myClassName";
		String columName = "myColumnName";
		long companyId = 123;
		when(mockExpandoField.getName()).thenReturn(columName);
		when(mockExpandoColumnLocalService.getColumn(companyId, className, ExpandoTableConstants.DEFAULT_TABLE_NAME, columName)).thenReturn(null);

		Optional<ExpandoColumn> result = expandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getExistingExpandoColumn_WhenExpandoColumnIsFoundAndTheTypeMatches_ThenReturnsOptionalWithExpandoColumn() throws ExpandoColumnCreationException {
		String className = "myClassName";
		String columName = "myColumnName";
		long companyId = 123;
		Integer type = 44;
		when(mockExpandoField.getName()).thenReturn(columName);
		when(mockExpandoColumn.getType()).thenReturn(type);
		when(mockExpandoField.getType()).thenReturn(type);
		when(mockExpandoColumnLocalService.getColumn(companyId, className, ExpandoTableConstants.DEFAULT_TABLE_NAME, columName)).thenReturn(mockExpandoColumn);

		Optional<ExpandoColumn> result = expandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField);

		assertThat(result.get(), sameInstance(mockExpandoColumn));
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void getExistingExpandoColumn_WhenExpandoColumnIsFoundAndTheTypeDoesNotMatch_ThenThrowsExpandoColumnCreationException() throws ExpandoColumnCreationException {
		String className = "myClassName";
		String columName = "myColumnName";
		long companyId = 123;
		Integer type = 44;
		Integer typeColumn = 441;
		when(mockExpandoField.getName()).thenReturn(columName);
		when(mockExpandoColumn.getType()).thenReturn(typeColumn);
		when(mockExpandoField.getType()).thenReturn(type);
		when(mockExpandoColumnLocalService.getColumn(companyId, className, ExpandoTableConstants.DEFAULT_TABLE_NAME, columName)).thenReturn(mockExpandoColumn);

		expandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField);
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void getExpandoFieldModelFromInputStream_WhenInputStreamCannotBeParsed_ThenThrowsExpandoColumnCreationException() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("expando_invalid_definition.xml");

		expandoCreationService.getExpandoFieldModelFromInputStream(inputStreamFile);
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void getExpandoFieldModelFromInputStream_WhenInputStreamCanBeParsedAndFieldNameIsInvalid_ThenThrowsExpandoColumnCreationException() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("expando_invalid_name.xml");

		expandoCreationService.getExpandoFieldModelFromInputStream(inputStreamFile);
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void getExpandoFieldModelFromInputStream_WhenInputStreamCanBeParsedAndTypeIsInvalid_ThenThrowsExpandoColumnCreationException() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("expando_invalid_no_type.xml");

		expandoCreationService.getExpandoFieldModelFromInputStream(inputStreamFile);
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void getExpandoFieldModelFromInputStream_WhenInputStreamCanBeParsedAndFieldNameIsNotGreaterThanZero_ThenThrowsExpandoColumnCreationException() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("expando_invalid_type.xml");

		expandoCreationService.getExpandoFieldModelFromInputStream(inputStreamFile);
	}

	@Test
	@Ignore
	public void getExpandoFieldModelFromInputStream_WhenInputStreamCanBeParsedToAvalidExpandoFieldModel_ThenReturnsTheExpandoFieldModel() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("expando_valid.xml");

		ExpandoField result = expandoCreationService.getExpandoFieldModelFromInputStream(inputStreamFile);

		assertThat(result.getName(), equalTo("ValidExpandoField"));
		assertThat(result.getType(), equalTo(Integer.valueOf("11")));
		assertThat(result.isHidden(), equalTo(false));
	}

	@Test
	@Ignore
	public void getExpandoFieldModelFromInputStream_WhenNoError_ThenCleanUpTheStream() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("expando_valid.xml");

		expandoCreationService.getExpandoFieldModelFromInputStream(inputStreamFile);

		verifyStatic(StreamUtil.class,times(1));
		StreamUtil.cleanUp(true, inputStreamFile);
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void getExpandoFieldModelFromInputStream_WhenException_ThenCleanUpTheStream() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("expando_invalid_definition.xml");

		expandoCreationService.getExpandoFieldModelFromInputStream(inputStreamFile);

		verifyStatic(StreamUtil.class,times(1));
		StreamUtil.cleanUp(true, inputStreamFile);
	}

	private InputStream getInputStreamFile(String fileName) throws FileNotFoundException {
		URL resource = Thread.currentThread().getContextClassLoader().getResource(fileName);
		return new FileInputStream(resource.getPath());
	}
}
