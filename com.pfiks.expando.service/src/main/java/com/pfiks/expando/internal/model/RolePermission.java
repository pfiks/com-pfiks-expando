/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.internal.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rolePermission", propOrder = { "view", "update" })
public class RolePermission {

	@XmlElement(name = "view", defaultValue = "true")
	protected boolean view;

	@XmlElement(name = "update", defaultValue = "true")
	protected boolean update;

	@XmlAttribute(name = "roleName", required = true)
	protected String roleName;

	public boolean isView() {
		return view;
	}

	public boolean isUpdate() {
		return update;
	}

	public String getRoleName() {
		return roleName;
	}

}
