/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.creator.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.Map;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.pfiks.expando.internal.model.ExpandoField;
import com.pfiks.expando.internal.service.CreationUtil;
import com.pfiks.expando.internal.service.ExpandoCreationService;

public class ExpandoColumnCreatorServiceImplTest extends PowerMockito {

	@InjectMocks
	private ExpandoColumnCreatorServiceImpl expandoColumnCreatorServiceImpl;

	@Mock
	private CreationUtil mockCreationUtil;

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Mock
	private ExpandoCreationService mockExpandoCreationService;

	@Mock
	private Map<String, String[]> mockRolesPermissions;

	@Mock
	private ExpandoField mockExpandoField;

	@Before
	public void setUp() {

		initMocks(this);

	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void createMissingExpandoColumn_WhenExceptionAddingColumn_ThenExpandoColumnCreationExceptionIsThrown() throws Exception {

		String fieldName = "myFieldName";
		String className = "myClassName";
		long companyId = 123;
		String columnSettings = "index-type=1";

		when(mockExpandoCreationService.createExpandoField(fieldName, ExpandoColumnConstants.STRING, StringPool.BLANK)).thenReturn(mockExpandoField);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.empty());
		when(mockExpandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField)).thenThrow(new ExpandoColumnCreationException(""));

		expandoColumnCreatorServiceImpl.createMissingExpandoColumn(companyId, className, fieldName, ExpandoColumnConstants.STRING, StringPool.BLANK, columnSettings, mockRolesPermissions);

	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void createMissingExpandoColumn_WhenExceptionSettingColumnSettins_ThenExpandoColumnCreationExceptionIsThrown() throws Exception {

		String fieldName = "myFieldName";
		String className = "myClassName";
		long companyId = 123;
		String columnSettings = "index-type=1";

		when(mockExpandoCreationService.createExpandoField(fieldName, ExpandoColumnConstants.STRING, StringPool.BLANK)).thenReturn(mockExpandoField);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.empty());
		when(mockExpandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(mockExpandoColumn);

		doThrow(new PortalException()).when(mockCreationUtil).setColumnSettings(mockExpandoColumn, columnSettings);

		expandoColumnCreatorServiceImpl.createMissingExpandoColumn(companyId, className, fieldName, ExpandoColumnConstants.STRING, StringPool.BLANK, columnSettings, mockRolesPermissions);

	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void createMissingExpandoColumn_WhenExceptionSettingColumnPermissions_ThenExpandoColumnCreationExceptionIsThrown() throws Exception {

		String fieldName = "myFieldName";
		String className = "myClassName";
		long companyId = 123;
		String columnSettings = "index-type=1";

		when(mockExpandoCreationService.createExpandoField(fieldName, ExpandoColumnConstants.STRING, StringPool.BLANK)).thenReturn(mockExpandoField);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.empty());
		when(mockExpandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(mockExpandoColumn);

		doThrow(new PortalException()).when(mockCreationUtil).setColumnPermissions(mockExpandoColumn, mockRolesPermissions);

		expandoColumnCreatorServiceImpl.createMissingExpandoColumn(companyId, className, fieldName, ExpandoColumnConstants.STRING, StringPool.BLANK, columnSettings, mockRolesPermissions);

	}

	@Test
	public void createMissingExpandoColumn_WhenExpandoColumnAlreadyExistsWithTheSameColumnName_ThenReturnsExistingExpandoColumn() throws Exception {

		String fieldName = "myFieldName";
		String className = "myClassName";
		long companyId = 123;
		String columnSettings = "index-type=1";

		when(mockExpandoCreationService.createExpandoField(fieldName, ExpandoColumnConstants.STRING, StringPool.BLANK)).thenReturn(mockExpandoField);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.of(mockExpandoColumn));

		ExpandoColumn result = expandoColumnCreatorServiceImpl.createMissingExpandoColumn(companyId, className, fieldName, ExpandoColumnConstants.STRING, StringPool.BLANK, columnSettings,
				mockRolesPermissions);

		assertThat(result, sameInstance(mockExpandoColumn));

	}

	@Test
	public void createMissingExpandoColumn_WhenNoError_ThenExpandoColumnIsCreatedAndConfigured() throws Exception {

		String fieldName = "myFieldName";
		String className = "myClassName";
		long companyId = 123;
		String columnSettings = "index-type=1";

		when(mockExpandoCreationService.createExpandoField(fieldName, ExpandoColumnConstants.STRING, StringPool.BLANK)).thenReturn(mockExpandoField);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.empty());
		when(mockExpandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(mockExpandoColumn);

		expandoColumnCreatorServiceImpl.createMissingExpandoColumn(companyId, className, fieldName, ExpandoColumnConstants.STRING, StringPool.BLANK, columnSettings, mockRolesPermissions);

		verify(mockCreationUtil, times(1)).setColumnSettings(mockExpandoColumn, columnSettings);
		verify(mockCreationUtil, times(1)).setColumnPermissions(mockExpandoColumn, mockRolesPermissions);

	}
}
