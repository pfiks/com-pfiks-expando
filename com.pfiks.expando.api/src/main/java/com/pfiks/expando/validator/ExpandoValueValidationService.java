/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.validator;

import java.io.Serializable;

@FunctionalInterface
public interface ExpandoValueValidationService {

	/**
	 * Validates if the value is accepted for the type
	 *
	 * @param type expando field type
	 * @param expandoValue expando value
	 * @return true if the value is accepted for the given type, false otherwise
	 */
	boolean isValueValidForColumn(int type, Serializable expandoValue);

}
