/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.internal.service;

import java.io.InputStream;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoTable;
import com.liferay.expando.kernel.model.ExpandoTableConstants;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StreamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.pfiks.expando.internal.model.ExpandoField;

@Component(immediate = true, service = ExpandoCreationService.class)
public class ExpandoCreationService {

	private static final Log LOG = LogFactoryUtil.getLog(ExpandoCreationService.class);

	@Reference
	private CreationUtil creationUtil;

	@Reference
	private ExpandoColumnLocalService expandoColumnLocalService;

	public ExpandoField createExpandoField(String fieldName, int columnType, String defaultValue) {
		return new ExpandoField(fieldName, columnType, defaultValue);
	}

	public ExpandoColumn createMissingExpandoColumn(long companyId, String className, ExpandoField expandoField) throws ExpandoColumnCreationException {
		try {
			ExpandoColumn expandoColumn = createColumn(companyId, className, expandoField);
			creationUtil.setColumnSettings(expandoColumn, creationUtil.getColumnTypeSettings(expandoField));
			creationUtil.setColumnPermissions(expandoColumn, creationUtil.getRolePermissions(expandoField));
			LOG.info("Expando column created - companyId: " + companyId + ", className: " + className + ", columnName: " + expandoColumn.getName());
			return expandoColumn;
		} catch (Exception e) {
			throw new ExpandoColumnCreationException(e);
		}
	}

	public Optional<ExpandoColumn> getExistingExpandoColumn(long companyId, String className, ExpandoField expandoField) throws ExpandoColumnCreationException {
		ExpandoColumn result = expandoColumnLocalService.getColumn(companyId, className, ExpandoTableConstants.DEFAULT_TABLE_NAME, expandoField.getName());
		if (Validator.isNotNull(result) && result.getType() != expandoField.getType()) {
			throw new ExpandoColumnCreationException("Column type does not match");
		}
		return Optional.ofNullable(result);
	}

	public ExpandoField getExpandoFieldModelFromInputStream(InputStream inputStream) throws ExpandoColumnCreationException {
		try {
			JAXBContext jc = JAXBContext.newInstance(ExpandoField.class);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			ExpandoField result = (ExpandoField) unmarshaller.unmarshal(inputStream);
			validateExpandoField(result);
			return result;
		} catch (JAXBException e) {
			throw new ExpandoColumnCreationException(e);
		} finally {
			StreamUtil.cleanUp(true, inputStream);
		}
	}

	private ExpandoColumn createColumn(long companyId, String className, ExpandoField expandoField) throws PortalException {
		ExpandoTable expandoTable = creationUtil.getOrCreateDefaultTableForClass(companyId, className);
		ExpandoColumn expandoColumn = expandoColumnLocalService.addColumn(expandoTable.getTableId(), expandoField.getName(), expandoField.getType(), creationUtil.getDefaultValue(expandoField));
		LOG.info("Created Expando column with columnId: " + expandoColumn.getColumnId() + " and name: " + expandoColumn.getName() + " in companyId: " + expandoColumn.getCompanyId());
		return expandoColumn;
	}

	private void validateExpandoField(ExpandoField expandoToImport) throws ExpandoColumnCreationException {
		if (Validator.isNull(expandoToImport.getName()) || Validator.isNull(expandoToImport.getType()) || expandoToImport.getType() <= 0) {
			throw new ExpandoColumnCreationException("Missing required values");
		}
	}

}
