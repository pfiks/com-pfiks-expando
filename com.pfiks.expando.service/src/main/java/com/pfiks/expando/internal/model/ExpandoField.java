/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.internal.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.liferay.portal.kernel.util.Validator;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "name", "type", "hidden", "visibleOnUpdate", "displayType", "defaultValue", "rolePermission" })
@XmlRootElement(name = "expandoField")
public class ExpandoField {

	@XmlElement(name = "name", required = true)
	protected String name;

	@XmlElement(name = "type", required = true)
	protected int type;

	@XmlElement(name = "hidden", defaultValue = "true")
	protected Boolean hidden;

	@XmlElement(name = "visibleOnUpdate", defaultValue = "false")
	protected Boolean visibleOnUpdate;

	@XmlElement(name = "displayType")
	protected String displayType;

	@XmlElement(name = "defaultValue")
	protected String defaultValue;

	@XmlElement(name = "rolePermission", required = true)
	protected List<RolePermission> rolePermission;

	public ExpandoField() {

	}

	public ExpandoField(String name, int type, String defaultValue) {

		this.name = name;
		this.type = type;
		this.defaultValue = defaultValue;

	}

	public String getName() {
		return name;
	}

	public int getType() {
		return type;
	}

	public Boolean isHidden() {
		return Validator.isNull(hidden) ? Boolean.TRUE : hidden;
	}

	public Boolean isVisibleOnUpdate() {
		return Validator.isNull(visibleOnUpdate) ? Boolean.FALSE : visibleOnUpdate;
	}

	public String getDisplayType() {
		return displayType;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public List<RolePermission> getRolePermission() {
		return null == rolePermission ? new ArrayList<>() : rolePermission;
	}

}
