/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.internal.service;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.expando.kernel.exception.NoSuchTableException;
import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.expando.kernel.model.ExpandoTable;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.expando.kernel.service.ExpandoTableLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.pfiks.expando.internal.model.ExpandoField;
import com.pfiks.expando.internal.model.RolePermission;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class CreationUtilTest extends PowerMockito {

	@InjectMocks
	private CreationUtil creationUtil;

	@Mock
	private CounterLocalService mockCounterLocalService;

	@Mock
	private ExpandoColumnLocalService mockExpandoColumnLocalService;

	@Mock
	private ExpandoTableLocalService mockExpandoTableLocalService;

	@Mock
	private ResourcePermissionLocalService mockResourcePermissionLocalService;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Mock
	private ExpandoField mockExpandoField;

	@Mock
	private ExpandoTable mockExpandoTable;

	@Mock
	private RolePermission mockRolePermissionModel1;

	@Mock
	private RolePermission mockRolePermissionModel2;

	@Mock
	private RolePermission mockRolePermissionModel3;

	@Mock
	private RolePermission mockRolePermissionModel4;

	@Mock
	private Role mockRole;

	@Mock
	private ResourcePermission mockResourcePermission;

	@Test
	@Parameters({ "true", "false" })
	public void getColumnTypeSettings_WhenPropertyHiddenIsFalse_ThenReturnsColumnTypeSettingsWithPropertyHiddenEqualTo0(boolean visibleOnUpdate) {
		when(mockExpandoField.isHidden()).thenReturn(false);
		when(mockExpandoField.isVisibleOnUpdate()).thenReturn(visibleOnUpdate);

		String result = creationUtil.getColumnTypeSettings(mockExpandoField);

		assertThat(result.contains(ExpandoColumnConstants.PROPERTY_HIDDEN + "=0"), equalTo(true));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getColumnTypeSettings_WhenPropertyHiddenIsTrue_ThenReturnsColumnTypeSettingsWithPropertyHiddenEqualTo1(boolean visibleOnUpdate) {
		when(mockExpandoField.isHidden()).thenReturn(true);
		when(mockExpandoField.isVisibleOnUpdate()).thenReturn(visibleOnUpdate);

		String result = creationUtil.getColumnTypeSettings(mockExpandoField);

		assertThat(result.contains(ExpandoColumnConstants.PROPERTY_HIDDEN + "=1"), equalTo(true));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getColumnTypeSettings_WhenPropertyVisibleOnUpdateIsFalse_ThenReturnsColumnTypeSettingsWithPropertyVisibleWithUpdatePermissionEqualTo0(boolean hidden) {
		when(mockExpandoField.isVisibleOnUpdate()).thenReturn(false);
		when(mockExpandoField.isHidden()).thenReturn(hidden);

		String result = creationUtil.getColumnTypeSettings(mockExpandoField);

		assertThat(result.contains(ExpandoColumnConstants.PROPERTY_VISIBLE_WITH_UPDATE_PERMISSION + "=0"), equalTo(true));
	}

	@Test
	@Parameters({ "true", "false" })
	public void getColumnTypeSettings_WhenPropertyVisibleOnUpdateIsTrue_ThenReturnsColumnTypeSettingsWithPropertyVisibleWithUpdatePermissionEqualTo1(boolean hidden) {
		when(mockExpandoField.isVisibleOnUpdate()).thenReturn(true);
		when(mockExpandoField.isHidden()).thenReturn(hidden);

		String result = creationUtil.getColumnTypeSettings(mockExpandoField);

		assertThat(result.contains(ExpandoColumnConstants.PROPERTY_VISIBLE_WITH_UPDATE_PERMISSION + "=1"), equalTo(true));
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,true", "false,false" })
	public void getColumnTypeSettings_WhenDisplayTypeIsNull_ThenReturnsColumnTypeSettingsWithIndexTypeAndPropertyHiddenAndPropertyVisibleWithUpdatePermission(boolean hidden, boolean visibleOnUpdate) {
		when(mockExpandoField.getDisplayType()).thenReturn(null);
		when(mockExpandoField.isHidden()).thenReturn(hidden);
		when(mockExpandoField.isVisibleOnUpdate()).thenReturn(visibleOnUpdate);
		String hiddenValue = hidden ? "1" : "0";
		String updateValue = visibleOnUpdate ? "1" : "0";
		String expected = "index-type=2\nhidden=" + hiddenValue + "\nvisible-with-update-permission=" + updateValue + "\n";

		String result = creationUtil.getColumnTypeSettings(mockExpandoField);

		assertThat(result, equalTo(expected));
	}

	@Test
	@Parameters({ "true,true", "true,false", "false,true", "false,false" })
	public void getColumnTypeSettings_WhenDisplayTypeIsSet_ThenReturnsColumnTypeSettingsWithIndexTypeAndPropertyHiddenAndPropertyVisibleWithUpdatePermissionAndPropertyDisplayType(boolean hidden,
			boolean visibleOnUpdate) {
		when(mockExpandoField.getDisplayType()).thenReturn("displayTypeValue");
		when(mockExpandoField.isHidden()).thenReturn(hidden);
		when(mockExpandoField.isVisibleOnUpdate()).thenReturn(visibleOnUpdate);
		String hiddenValue = hidden ? "1" : "0";
		String updateValue = visibleOnUpdate ? "1" : "0";
		String expected = "index-type=2\nhidden=" + hiddenValue + "\nvisible-with-update-permission=" + updateValue + "\ndisplay-type=displayTypeValue";

		String result = creationUtil.getColumnTypeSettings(mockExpandoField);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getDefaultValue_WhenDefaultValueIsNull_ThenReturnsNull() {
		when(mockExpandoField.getDefaultValue()).thenReturn(null);

		Object result = creationUtil.getDefaultValue(mockExpandoField);

		assertThat(result, nullValue());
	}

	@Test
	@Parameters({ "true", "false" })
	public void getDefaultValue_WhenDefaultValueIsValidAndColumnTypeIsBoolean_ThenReturnsTheValueAsBoolean(String myValue) {
		when(mockExpandoField.getDefaultValue()).thenReturn(myValue);
		when(mockExpandoField.getType()).thenReturn(ExpandoColumnConstants.BOOLEAN);

		Object result = creationUtil.getDefaultValue(mockExpandoField);

		assertThat(result, equalTo(Boolean.valueOf(myValue)));
	}

	@Test
	public void getDefaultValue_WhenDefaultValueIsValidAndColumnTypeIsString_ThenReturnsTheValue() {
		String myValue = "myValue";
		when(mockExpandoField.getDefaultValue()).thenReturn(myValue);
		when(mockExpandoField.getType()).thenReturn(ExpandoColumnConstants.STRING);

		Object result = creationUtil.getDefaultValue(mockExpandoField);

		assertThat(result, equalTo(myValue));
	}

	@Test
	public void getDefaultValue_WhenDefaultValueIsValidAndColumnTypeIsStringArray_ThenReturnsTheValueAsStringArraySplittedByComma() {
		String myValue = "myValueOne,myValueTwo";
		when(mockExpandoField.getDefaultValue()).thenReturn(myValue);
		when(mockExpandoField.getType()).thenReturn(ExpandoColumnConstants.STRING_ARRAY);

		Object result = creationUtil.getDefaultValue(mockExpandoField);

		assertThat(result, equalTo(new String[] { "myValueOne", "myValueTwo" }));
	}

	@Test
	public void getDefaultValue_WhenDefaultValueIsValidAndTypeIsNotHandled_ThenReturnsNull() {
		when(mockExpandoField.getDefaultValue()).thenReturn("myValue");
		when(mockExpandoField.getType()).thenReturn(ExpandoColumnConstants.DATE);

		Object result = creationUtil.getDefaultValue(mockExpandoField);

		assertThat(result, nullValue());
	}

	@Test
	public void getOrCreateDefaultTableForClass_WhenTheDefaultTableForTheClassnameAlreadyExist_ThenDoesNotCreateTheTable() throws Exception {
		long companyId = 123;
		String className = "classNameValue";
		when(mockExpandoTableLocalService.getDefaultTable(companyId, className)).thenReturn(mockExpandoTable);

		creationUtil.getOrCreateDefaultTableForClass(companyId, className);

		verify(mockExpandoTableLocalService, never()).addDefaultTable(anyLong(), anyString());
	}

	@Test
	public void getOrCreateDefaultTableForClass_WhenTheDefaultTableForTheClassnameAlreadyExist_ThenReturnsTheTableFound() throws Exception {
		long companyId = 123;
		String className = "classNameValue";
		when(mockExpandoTableLocalService.getDefaultTable(companyId, className)).thenReturn(mockExpandoTable);

		ExpandoTable result = creationUtil.getOrCreateDefaultTableForClass(companyId, className);

		assertThat(result, sameInstance(mockExpandoTable));
	}

	@Test
	public void getOrCreateDefaultTableForClass_WhenTheDefaultTableForTheClassnameDoesNotExist_ThenReturnsTheNewlyCreatedTable() throws Exception {
		long companyId = 123;
		String className = "classNameValue";
		when(mockExpandoTableLocalService.getDefaultTable(companyId, className)).thenThrow(new NoSuchTableException());
		when(mockExpandoTableLocalService.addDefaultTable(companyId, className)).thenReturn(mockExpandoTable);

		ExpandoTable result = creationUtil.getOrCreateDefaultTableForClass(companyId, className);

		assertThat(result, sameInstance(mockExpandoTable));
	}

	@Test
	public void getRolePermissions_WhenNoError_ThenReturnsMapWithRolesAndRelevantViewUpdatePermission() {
		List<RolePermission> rolePermissions = new ArrayList<>();
		rolePermissions.add(mockRolePermissionModel1);
		rolePermissions.add(mockRolePermissionModel2);
		rolePermissions.add(mockRolePermissionModel3);
		rolePermissions.add(mockRolePermissionModel4);
		when(mockExpandoField.getRolePermission()).thenReturn(rolePermissions);

		mockRolePermissionDetails(mockRolePermissionModel1, "roleName1", false, true);
		mockRolePermissionDetails(mockRolePermissionModel2, "roleName2", true, false);
		mockRolePermissionDetails(mockRolePermissionModel3, "roleName3", false, false);
		mockRolePermissionDetails(mockRolePermissionModel4, "roleName4", true, true);

		Map<String, String[]> results = creationUtil.getRolePermissions(mockExpandoField);

		assertThat(results.size(), equalTo(3));
		assertThat(results.get("roleName1"), equalTo(new String[] { ActionKeys.VIEW }));
		assertThat(results.get("roleName2"), equalTo(new String[] { ActionKeys.UPDATE }));
		assertThat(results.get("roleName4"), equalTo(new String[] { ActionKeys.UPDATE, ActionKeys.VIEW }));
	}

	@Test
	@Parameters({ "", "null" })
	public void configureColumnPermissionForRole_WhenRoleNameIsInvalid_ThenNoActionIsPerformed(String roleName) throws Exception {
		creationUtil.configureColumnPermissionForRole(1, 2, roleName, new String[] { "one", "two" });

		verifyZeroInteractions(mockResourcePermissionLocalService, mockRoleLocalService);
	}

	@Test
	public void configureColumnPermissionForRole_WhenActionKeysAreInvalid_ThenNoActionIsPerformed() throws Exception {
		creationUtil.configureColumnPermissionForRole(1, 2, "roleName", new String[] {});

		verifyZeroInteractions(mockResourcePermissionLocalService, mockRoleLocalService);
	}

	@Test
	public void configureColumnPermissionForRole_WhenValidValuesAndResourcePermissionDoesNotExistForTheGivenRole_ThenAnewResourcePermissionIsCreatedAndConfigured() throws Exception {
		long companyId = 123;
		long columnId = 456;
		long roleId = 789;
		Integer permissionScope = 454;
		Long permissionRoleId = 77l;
		Long nextId = 33l;
		String roleName = "RoleOne";
		String permissionName = "permissionName";
		String permissionPrimKey = "permissionPrimKey";
		String[] actionKeysValues = new String[] { "permissionA", "permissionB" };

		when(mockRoleLocalService.getRole(companyId, roleName)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(roleId);
		when(mockResourcePermissionLocalService.getResourcePermission(companyId, ExpandoColumn.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(columnId), roleId))
				.thenThrow(new PortalException());
		when(mockCounterLocalService.increment(ResourcePermission.class.getName())).thenReturn(nextId);
		when(mockResourcePermissionLocalService.createResourcePermission(nextId)).thenReturn(mockResourcePermission);
		when(mockResourcePermission.getName()).thenReturn(permissionName);
		when(mockResourcePermission.getScope()).thenReturn(permissionScope);
		when(mockResourcePermission.getPrimKey()).thenReturn(permissionPrimKey);
		when(mockResourcePermission.getRoleId()).thenReturn(permissionRoleId);
		when(mockResourcePermission.getCompanyId()).thenReturn(companyId);

		creationUtil.configureColumnPermissionForRole(companyId, columnId, roleName, actionKeysValues);

		InOrder inOrder = inOrder(mockResourcePermission, mockResourcePermissionLocalService);
		inOrder.verify(mockResourcePermission, times(1)).setActionIds(1);
		inOrder.verify(mockResourcePermission, times(1)).setCompanyId(companyId);
		inOrder.verify(mockResourcePermission, times(1)).setName(ExpandoColumn.class.getName());
		inOrder.verify(mockResourcePermission, times(1)).setPrimKey(String.valueOf(columnId));
		inOrder.verify(mockResourcePermission, times(1)).setRoleId(roleId);
		inOrder.verify(mockResourcePermission, times(1)).setScope(ResourceConstants.SCOPE_INDIVIDUAL);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).updateResourcePermission(mockResourcePermission);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).setResourcePermissions(companyId, permissionName, permissionScope, permissionPrimKey, permissionRoleId, actionKeysValues);
	}

	@Test
	public void configureColumnPermissionForRole_WhenValidValuesAndResourcePermissionAlreadyExistForTheGivenRole_ThenTheResourcePermissionIsConfigured() throws Exception {
		long companyId = 123;
		long columnId = 456;
		long roleId = 789;
		Integer permissionScope = 454;
		Long permissionRoleId = 77l;
		String roleName = "RoleOne";
		String permissionName = "permissionName";
		String permissionPrimKey = "permissionPrimKey";
		String[] actionKeysValues = new String[] { "permissionA", "permissionB" };

		when(mockRoleLocalService.getRole(companyId, roleName)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(roleId);
		when(mockResourcePermissionLocalService.getResourcePermission(companyId, ExpandoColumn.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(columnId), roleId))
				.thenReturn(mockResourcePermission);
		when(mockResourcePermission.getName()).thenReturn(permissionName);
		when(mockResourcePermission.getScope()).thenReturn(permissionScope);
		when(mockResourcePermission.getPrimKey()).thenReturn(permissionPrimKey);
		when(mockResourcePermission.getRoleId()).thenReturn(permissionRoleId);
		when(mockResourcePermission.getCompanyId()).thenReturn(companyId);

		creationUtil.configureColumnPermissionForRole(companyId, columnId, roleName, actionKeysValues);

		InOrder inOrder = inOrder(mockResourcePermission, mockResourcePermissionLocalService);
		inOrder.verify(mockResourcePermission, times(1)).setActionIds(1);
		inOrder.verify(mockResourcePermission, times(1)).setCompanyId(companyId);
		inOrder.verify(mockResourcePermission, times(1)).setName(ExpandoColumn.class.getName());
		inOrder.verify(mockResourcePermission, times(1)).setPrimKey(String.valueOf(columnId));
		inOrder.verify(mockResourcePermission, times(1)).setRoleId(roleId);
		inOrder.verify(mockResourcePermission, times(1)).setScope(ResourceConstants.SCOPE_INDIVIDUAL);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).updateResourcePermission(mockResourcePermission);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).setResourcePermissions(companyId, permissionName, permissionScope, permissionPrimKey, permissionRoleId, actionKeysValues);
		verifyZeroInteractions(mockCounterLocalService);
	}

	@Test
	public void setColumnPermissions_WhenRolesPermissionsIsEmpty_ThenNoActionIsPerformed() throws Exception {

		Map<String, String[]> rolesPermissions = new HashMap<>();

		creationUtil.setColumnPermissions(mockExpandoColumn, rolesPermissions);

		verifyZeroInteractions(mockResourcePermissionLocalService, mockRoleLocalService);
	}

	@Test
	public void setColumnPermissions_WhenRolesPermissionsIsNull_ThenNoActionIsPerformed() throws Exception {

		creationUtil.setColumnPermissions(mockExpandoColumn, null);

		verifyZeroInteractions(mockResourcePermissionLocalService, mockRoleLocalService);
	}

	@Test
	@Parameters({ "", "null" })
	public void setColumnPermissions_WhenRoleNameIsInvalid_ThenNoActionIsPerformed(String roleName) throws Exception {

		Map<String, String[]> rolesPermissions = new HashMap<>();
		rolesPermissions.put(roleName, new String[] { "one", "two" });

		creationUtil.setColumnPermissions(mockExpandoColumn, rolesPermissions);

		verifyZeroInteractions(mockResourcePermissionLocalService, mockRoleLocalService);
	}

	@Test
	public void setColumnPermissions_WhenActionKeysAreInvalid_ThenNoActionIsPerformed() throws Exception {

		Map<String, String[]> rolesPermissions = new HashMap<>();
		rolesPermissions.put("roleName", new String[] {});

		creationUtil.setColumnPermissions(mockExpandoColumn, rolesPermissions);

		verifyZeroInteractions(mockResourcePermissionLocalService, mockRoleLocalService);
	}

	@Test
	public void setColumnPermissions_WhenValidValuesAndResourcePermissionDoesNotExistForTheGivenRole_ThenAnewResourcePermissionIsCreatedAndConfigured() throws Exception {
		long companyId = 123;
		long columnId = 456;
		long roleId = 789;
		Integer permissionScope = 454;
		Long permissionRoleId = 77l;
		Long nextId = 33l;
		String roleName = "RoleOne";
		String permissionName = "permissionName";
		String permissionPrimKey = "permissionPrimKey";
		String[] actionKeysValues = new String[] { "permissionA", "permissionB" };

		Map<String, String[]> rolesPermissions = new HashMap<>();
		rolesPermissions.put(roleName, actionKeysValues);

		when(mockExpandoColumn.getCompanyId()).thenReturn(companyId);
		when(mockExpandoColumn.getColumnId()).thenReturn(columnId);
		when(mockRoleLocalService.getRole(companyId, roleName)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(roleId);
		when(mockResourcePermissionLocalService.getResourcePermission(companyId, ExpandoColumn.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(columnId), roleId))
				.thenThrow(new PortalException());
		when(mockCounterLocalService.increment(ResourcePermission.class.getName())).thenReturn(nextId);
		when(mockResourcePermissionLocalService.createResourcePermission(nextId)).thenReturn(mockResourcePermission);
		when(mockResourcePermission.getName()).thenReturn(permissionName);
		when(mockResourcePermission.getScope()).thenReturn(permissionScope);
		when(mockResourcePermission.getPrimKey()).thenReturn(permissionPrimKey);
		when(mockResourcePermission.getRoleId()).thenReturn(permissionRoleId);
		when(mockResourcePermission.getCompanyId()).thenReturn(companyId);

		creationUtil.setColumnPermissions(mockExpandoColumn, rolesPermissions);

		InOrder inOrder = inOrder(mockResourcePermission, mockResourcePermissionLocalService);
		inOrder.verify(mockResourcePermission, times(1)).setActionIds(1);
		inOrder.verify(mockResourcePermission, times(1)).setCompanyId(companyId);
		inOrder.verify(mockResourcePermission, times(1)).setName(ExpandoColumn.class.getName());
		inOrder.verify(mockResourcePermission, times(1)).setPrimKey(String.valueOf(columnId));
		inOrder.verify(mockResourcePermission, times(1)).setRoleId(roleId);
		inOrder.verify(mockResourcePermission, times(1)).setScope(ResourceConstants.SCOPE_INDIVIDUAL);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).updateResourcePermission(mockResourcePermission);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).setResourcePermissions(companyId, permissionName, permissionScope, permissionPrimKey, permissionRoleId, actionKeysValues);
	}

	@Test
	public void setColumnPermissions_WhenValidValuesAndResourcePermissionAlreadyExistForTheGivenRole_ThenTheResourcePermissionIsConfigured() throws Exception {
		long companyId = 123;
		long columnId = 456;
		long roleId = 789;
		Integer permissionScope = 454;
		Long permissionRoleId = 77l;
		String roleName = "RoleOne";
		String permissionName = "permissionName";
		String permissionPrimKey = "permissionPrimKey";
		String[] actionKeysValues = new String[] { "permissionA", "permissionB" };

		Map<String, String[]> rolesPermissions = new HashMap<>();
		rolesPermissions.put(roleName, actionKeysValues);

		when(mockExpandoColumn.getCompanyId()).thenReturn(companyId);
		when(mockExpandoColumn.getColumnId()).thenReturn(columnId);

		when(mockRoleLocalService.getRole(companyId, roleName)).thenReturn(mockRole);
		when(mockRole.getRoleId()).thenReturn(roleId);
		when(mockResourcePermissionLocalService.getResourcePermission(companyId, ExpandoColumn.class.getName(), ResourceConstants.SCOPE_INDIVIDUAL, String.valueOf(columnId), roleId))
				.thenReturn(mockResourcePermission);
		when(mockResourcePermission.getName()).thenReturn(permissionName);
		when(mockResourcePermission.getScope()).thenReturn(permissionScope);
		when(mockResourcePermission.getPrimKey()).thenReturn(permissionPrimKey);
		when(mockResourcePermission.getRoleId()).thenReturn(permissionRoleId);
		when(mockResourcePermission.getCompanyId()).thenReturn(companyId);

		creationUtil.setColumnPermissions(mockExpandoColumn, rolesPermissions);

		InOrder inOrder = inOrder(mockResourcePermission, mockResourcePermissionLocalService);
		inOrder.verify(mockResourcePermission, times(1)).setActionIds(1);
		inOrder.verify(mockResourcePermission, times(1)).setCompanyId(companyId);
		inOrder.verify(mockResourcePermission, times(1)).setName(ExpandoColumn.class.getName());
		inOrder.verify(mockResourcePermission, times(1)).setPrimKey(String.valueOf(columnId));
		inOrder.verify(mockResourcePermission, times(1)).setRoleId(roleId);
		inOrder.verify(mockResourcePermission, times(1)).setScope(ResourceConstants.SCOPE_INDIVIDUAL);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).updateResourcePermission(mockResourcePermission);
		inOrder.verify(mockResourcePermissionLocalService, times(1)).setResourcePermissions(companyId, permissionName, permissionScope, permissionPrimKey, permissionRoleId, actionKeysValues);
		verifyZeroInteractions(mockCounterLocalService);
	}

	@Test
	@Parameters({ "", "null" })
	public void setColumnSettings_WhenColumnSettingsIsNotValid_ThenExpandoColumnsSettingsIsNotUpdated(String columnSettings) throws Exception {

		long columnId = 456;
		when(mockExpandoColumn.getColumnId()).thenReturn(columnId);
		creationUtil.setColumnSettings(mockExpandoColumn, columnSettings);

		verify(mockExpandoColumnLocalService, never()).updateTypeSettings(columnId, columnSettings);
	}

	@Test
	public void setColumnSettings_WhenColumnSettingsIsValid_ThenExpandoColumnsSettingsIsUpdated() throws Exception {

		long columnId = 456;
		when(mockExpandoColumn.getColumnId()).thenReturn(columnId);
		String columnSettings = "index-type=1";

		creationUtil.setColumnSettings(mockExpandoColumn, columnSettings);

		verify(mockExpandoColumnLocalService, times(1)).updateTypeSettings(columnId, columnSettings);
	}

	private void mockRolePermissionDetails(RolePermission rolePermissionModel, String roleName, boolean isUpdate, boolean isView) {
		when(rolePermissionModel.getRoleName()).thenReturn(roleName);
		when(rolePermissionModel.isUpdate()).thenReturn(isUpdate);
		when(rolePermissionModel.isView()).thenReturn(isView);
	}
}
