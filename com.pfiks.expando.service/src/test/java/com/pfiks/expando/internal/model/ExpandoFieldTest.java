/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.internal.model;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ExpandoFieldTest {

	@Test
	public void newExpandoFieldModel_ThenInitializesHiddenToTrue() {
		ExpandoField result = new ExpandoField();

		assertTrue(result.isHidden());
	}

	@Test
	public void newExpandoFieldModel_ThenInitializesRolePermissionsToEmptyList() {
		ExpandoField result = new ExpandoField();

		assertNotNull(result.getRolePermission());
	}

	@Test
	public void newExpandoFieldModel_ThenInitializesVisibleOnUpdateToFalse() {
		ExpandoField result = new ExpandoField();

		assertFalse(result.isVisibleOnUpdate());
	}

}
