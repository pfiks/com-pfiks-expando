/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.creator.impl;

import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.InputStream;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.pfiks.expando.internal.model.ExpandoField;
import com.pfiks.expando.internal.service.ExpandoCreationService;

public class ExpandoColumnCreatorInputStreamServiceImplTest extends PowerMockito {

	@InjectMocks
	private ExpandoColumnCreatorInputStreamServiceImpl expandoColumnCreatorInputStreamServiceImpl;

	@Mock
	private ExpandoColumn mockExpandoColumn;

	@Mock
	private ExpandoCreationService mockExpandoCreationService;

	@Mock
	private ExpandoField mockExpandoField;

	@Mock
	private InputStream mockInputStream;

	@Mock
	private Company mockCompany;

	@Before
	public void setUp() {

		initMocks(this);

	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void createExpandoColumn_WithClassAndInputStreamPathAdditionalParameters_WhenExpandoFieldCannotBeRetrievedFromInputStream_ThenThrowsExpandoColumnCreationException() throws Exception {
		Class clazz = GroupLocalService.class;
		String inputStreamPath = "inputStreamPathValue";
		when(mockExpandoCreationService.getExpandoFieldModelFromInputStream(any())).thenThrow(new ExpandoColumnCreationException("msg"));

		expandoColumnCreatorInputStreamServiceImpl.createExpandoColumn(mockCompany, "myClassName", clazz, inputStreamPath);
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void createExpandoColumn_WithClassAndInputStreamPathAdditionalParameters_WhenExpandoFieldCannotBeRetrievedFromInputStream_ThenDoesNotRetrieveOrCreateColumn() throws Exception {
		Class clazz = GroupLocalService.class;
		String inputStreamPath = "inputStreamPathValue";
		when(mockExpandoCreationService.getExpandoFieldModelFromInputStream(any())).thenThrow(new ExpandoColumnCreationException("msg"));

		expandoColumnCreatorInputStreamServiceImpl.createExpandoColumn(mockCompany, "myClassName", clazz, inputStreamPath);

		verify(mockExpandoCreationService, never()).createMissingExpandoColumn(anyLong(), anyString(), any(ExpandoField.class));
	}

	@Test
	public void createExpandoColumn_WithClassAndInputStreamPathAdditionalParameters_WhenExpandoColumnAlreadyExists_ThenReturnsTheExpandoColumn() throws Exception {
		Class clazz = GroupLocalService.class;
		String inputStreamPath = "inputStreamPathValue";
		when(mockExpandoCreationService.getExpandoFieldModelFromInputStream(any())).thenReturn(mockExpandoField);
		String className = "myClassName";
		long companyId = 123;
		when(mockCompany.getCompanyId()).thenReturn(companyId);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.of(mockExpandoColumn));

		ExpandoColumn result = expandoColumnCreatorInputStreamServiceImpl.createExpandoColumn(mockCompany, className, clazz, inputStreamPath);

		assertThat(result, sameInstance(mockExpandoColumn));
	}

	@Test
	public void createExpandoColumn_WithClassAndInputStreamPathAdditionalParameters_WhenExpandoColumnAlreadyExists_ThenDoesNotCreateNewExpandoColumn() throws Exception {
		Class clazz = GroupLocalService.class;
		String inputStreamPath = "inputStreamPathValue";
		when(mockExpandoCreationService.getExpandoFieldModelFromInputStream(any())).thenReturn(mockExpandoField);
		String className = "myClassName";
		long companyId = 123;
		when(mockCompany.getCompanyId()).thenReturn(companyId);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.of(mockExpandoColumn));

		expandoColumnCreatorInputStreamServiceImpl.createExpandoColumn(mockCompany, className, clazz, inputStreamPath);

		verify(mockExpandoCreationService, never()).createMissingExpandoColumn(anyLong(), anyString(), any(ExpandoField.class));
	}

	@Test
	public void createExpandoColumn_WithClassAndInputStreamPathAdditionalParameters_WhenExpandoColumnDoesNotAlreadyExist_ThenCreatesNewExpandoColumnAndReturnsTheNewlyCreatedColumn() throws Exception {
		Class clazz = GroupLocalService.class;
		String inputStreamPath = "inputStreamPathValue";
		when(mockExpandoCreationService.getExpandoFieldModelFromInputStream(any())).thenReturn(mockExpandoField);
		String className = "myClassName";
		long companyId = 123;
		when(mockCompany.getCompanyId()).thenReturn(companyId);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.empty());
		when(mockExpandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(mockExpandoColumn);

		ExpandoColumn result = expandoColumnCreatorInputStreamServiceImpl.createExpandoColumn(mockCompany, className, clazz, inputStreamPath);

		assertThat(result, sameInstance(mockExpandoColumn));
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void createExpandoColumn_WhenExpandoFieldCannotBeRetrievedFromInputStream_ThenThrowsExpandoColumnCreationException() throws Exception {
		when(mockExpandoCreationService.getExpandoFieldModelFromInputStream(mockInputStream)).thenThrow(new ExpandoColumnCreationException("msg"));

		expandoColumnCreatorInputStreamServiceImpl.createExpandoColumn(mockCompany, "myClassName", mockInputStream);
	}

	@Test(expected = ExpandoColumnCreationException.class)
	public void createExpandoColumn_WhenExpandoFieldCannotBeRetrievedFromInputStream_ThenDoesNotRetrieveOrCreateColumn() throws Exception {
		when(mockExpandoCreationService.getExpandoFieldModelFromInputStream(mockInputStream)).thenThrow(new ExpandoColumnCreationException("msg"));

		expandoColumnCreatorInputStreamServiceImpl.createExpandoColumn(mockCompany, "myClassName", mockInputStream);

		verify(mockExpandoCreationService, never()).createMissingExpandoColumn(anyLong(), anyString(), any(ExpandoField.class));
	}

	@Test
	public void createExpandoColumn_WhenExpandoColumnAlreadyExists_ThenReturnsTheExpandoColumn() throws Exception {
		when(mockExpandoCreationService.getExpandoFieldModelFromInputStream(mockInputStream)).thenReturn(mockExpandoField);
		String className = "myClassName";
		long companyId = 123;
		when(mockCompany.getCompanyId()).thenReturn(companyId);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.of(mockExpandoColumn));

		ExpandoColumn result = expandoColumnCreatorInputStreamServiceImpl.createExpandoColumn(mockCompany, className, mockInputStream);

		assertThat(result, sameInstance(mockExpandoColumn));
	}

	@Test
	public void createExpandoColumn_WhenExpandoColumnAlreadyExists_ThenDoesNotCreateNewExpandoColumn() throws Exception {
		when(mockExpandoCreationService.getExpandoFieldModelFromInputStream(mockInputStream)).thenReturn(mockExpandoField);
		String className = "myClassName";
		long companyId = 123;
		when(mockCompany.getCompanyId()).thenReturn(companyId);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.of(mockExpandoColumn));

		expandoColumnCreatorInputStreamServiceImpl.createExpandoColumn(mockCompany, className, mockInputStream);

		verify(mockExpandoCreationService, never()).createMissingExpandoColumn(anyLong(), anyString(), any(ExpandoField.class));
	}

	@Test
	public void createExpandoColumn_WhenExpandoColumnDoesNotAlreadyExist_ThenCreatesNewExpandoColumnAndReturnsTheNewlyCreatedColumn() throws Exception {
		when(mockExpandoCreationService.getExpandoFieldModelFromInputStream(mockInputStream)).thenReturn(mockExpandoField);
		String className = "myClassName";
		long companyId = 123;
		when(mockCompany.getCompanyId()).thenReturn(companyId);
		when(mockExpandoCreationService.getExistingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(Optional.empty());
		when(mockExpandoCreationService.createMissingExpandoColumn(companyId, className, mockExpandoField)).thenReturn(mockExpandoColumn);

		ExpandoColumn result = expandoColumnCreatorInputStreamServiceImpl.createExpandoColumn(mockCompany, className, mockInputStream);

		assertThat(result, sameInstance(mockExpandoColumn));
	}
}
