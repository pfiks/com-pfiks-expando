/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.creator.impl;

import java.util.Map;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.pfiks.expando.creator.ExpandoColumnCreatorService;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.pfiks.expando.internal.model.ExpandoField;
import com.pfiks.expando.internal.service.CreationUtil;
import com.pfiks.expando.internal.service.ExpandoCreationService;

@Component(immediate = true, service = ExpandoColumnCreatorService.class)
public class ExpandoColumnCreatorServiceImpl implements ExpandoColumnCreatorService {

	private static final Log LOG = LogFactoryUtil.getLog(ExpandoColumnCreatorServiceImpl.class);

	@Reference
	private CreationUtil creationUtil;

	@Reference
	private ExpandoCreationService expandoCreationService;

	@Override
	public ExpandoColumn createMissingExpandoColumn(long companyId, String className, String fieldName, int columnType, Object defaultValue, String columnSettings,
			Map<String, String[]> rolesPermissions) throws ExpandoColumnCreationException {

		try {

			ExpandoField expandoField = expandoCreationService.createExpandoField(fieldName, columnType, (String) defaultValue);

			Optional<ExpandoColumn> existingExpandoColumn = expandoCreationService.getExistingExpandoColumn(companyId, className, expandoField);
			if (existingExpandoColumn.isPresent()) {
				return existingExpandoColumn.get();
			}

			ExpandoColumn expandoColumn = expandoCreationService.createMissingExpandoColumn(companyId, className, expandoField);
			creationUtil.setColumnSettings(expandoColumn, columnSettings);
			creationUtil.setColumnPermissions(expandoColumn, rolesPermissions);
			LOG.info("Expando column updated with settings and permissions - companyId: " + companyId + ", className: " + className + ", columnName: " + expandoColumn.getName());
			return expandoColumn;
		} catch (Exception e) {
			throw new ExpandoColumnCreationException("Exception creating missing expando column");
		}

	}
}
