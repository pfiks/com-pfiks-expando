/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.validator.impl;

import java.io.Serializable;
import java.util.Date;

import org.osgi.service.component.annotations.Component;

import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.pfiks.expando.validator.ExpandoValueValidationService;

import jodd.typeconverter.TypeConverterManager;

@Component(immediate = true, service = ExpandoValueValidationService.class)
public class ExpandoValueValidationServiceImpl implements ExpandoValueValidationService {

	private static final Log LOG = LogFactoryUtil.getLog(ExpandoValueValidationServiceImpl.class);

	@Override
	public boolean isValueValidForColumn(int type, Serializable expandoValue) {
		try {
			switch (type) {

			case ExpandoColumnConstants.BOOLEAN:
				TypeConverterManager.convertType(expandoValue, Boolean.TYPE);
				return true;

			case ExpandoColumnConstants.BOOLEAN_ARRAY:
				TypeConverterManager.convertType(expandoValue, boolean[].class);
				return true;

			case ExpandoColumnConstants.DATE:
				TypeConverterManager.convertType(expandoValue, Date.class);
				return true;

			case ExpandoColumnConstants.DATE_ARRAY:
				TypeConverterManager.convertType(expandoValue, Date[].class);
				return true;

			case ExpandoColumnConstants.DOUBLE:
				TypeConverterManager.convertType(expandoValue, Double.TYPE);
				return true;

			case ExpandoColumnConstants.DOUBLE_ARRAY:
				TypeConverterManager.convertType(expandoValue, double[].class);
				return true;

			case ExpandoColumnConstants.FLOAT:
				TypeConverterManager.convertType(expandoValue, Float.TYPE);
				return true;

			case ExpandoColumnConstants.FLOAT_ARRAY:
				TypeConverterManager.convertType(expandoValue, float[].class);
				return true;

			case ExpandoColumnConstants.INTEGER:
				TypeConverterManager.convertType(expandoValue, Integer.TYPE);
				return true;

			case ExpandoColumnConstants.INTEGER_ARRAY:
				TypeConverterManager.convertType(expandoValue, int[].class);
				return true;

			case ExpandoColumnConstants.LONG:
				TypeConverterManager.convertType(expandoValue, Long.TYPE);
				return true;

			case ExpandoColumnConstants.LONG_ARRAY:
				TypeConverterManager.convertType(expandoValue, long[].class);
				return true;

			case ExpandoColumnConstants.NUMBER:
				TypeConverterManager.convertType(expandoValue, Number.class);
				return true;

			case ExpandoColumnConstants.NUMBER_ARRAY:
				TypeConverterManager.convertType(expandoValue, Number[].class);
				return true;

			case ExpandoColumnConstants.SHORT:
				TypeConverterManager.convertType(expandoValue, Short.TYPE);
				return true;

			case ExpandoColumnConstants.SHORT_ARRAY:
				TypeConverterManager.convertType(expandoValue, short[].class);
				return true;

			case ExpandoColumnConstants.STRING:
				TypeConverterManager.convertType(expandoValue, String.class);
				return true;

			case ExpandoColumnConstants.STRING_ARRAY:
				TypeConverterManager.convertType(expandoValue, String[].class);
				return true;

			default:
				return false;
			}
		} catch (Exception e) {
			LOG.debug("Unable to convert expando value", e);
			return false;
		}
	}

}
