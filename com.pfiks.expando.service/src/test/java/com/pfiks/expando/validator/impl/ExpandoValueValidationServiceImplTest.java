/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.validator.impl;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;

import com.liferay.expando.kernel.model.ExpandoColumnConstants;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(JUnitParamsRunner.class)
public class ExpandoValueValidationServiceImplTest {

	private ExpandoValueValidationServiceImpl expandoValueValidationServiceImpl = new ExpandoValueValidationServiceImpl();

	@Test
	@Parameters({ "true", "false" })
	public void isValueValidForColumn_WhenTypeIsBooleanAndValueIsBoolean_ThenReturnsTrue(boolean booleanValue) {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.BOOLEAN, booleanValue);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsBooleanAndValueIsNotBoolean_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.BOOLEAN, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsBooleanArrayAndValueIsBooleanArray_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.BOOLEAN_ARRAY, new boolean[] { true, false });

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsBooleanArrayAndValueIsNotBooleanArray_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.BOOLEAN_ARRAY, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsDateAndValueIsDate_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.DATE, new Date());

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsDateAndValueIsNotDate_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.DATE, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsDateArrayAndValueIsDateArray_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.DATE_ARRAY, new Date[] { new Date(), new Date() });

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsDateArrayAndValueIsNotDateArray_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.DATE_ARRAY, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsDoubleAndValueIsDouble_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.DOUBLE, 55.36);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsDoubleAndValueIsNotDouble_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.DOUBLE, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsDoubleArrayAndValueIsDoubleArray_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.DOUBLE_ARRAY, new Double[] { 22.1, 13.6 });

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsDoubleArrayAndValueIsNotDoubleArray_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.DOUBLE_ARRAY, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsFloatAndValueIsFloat_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.FLOAT, 55.36F);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsFloatAndValueIsNotFloat_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.FLOAT, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsFloatArrayAndValueIsFloatArray_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.FLOAT_ARRAY, new Float[] { 22.1F, 13.6F });

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsFloatArrayAndValueIsNotFloatArray_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.FLOAT_ARRAY, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsIntegerAndValueIsInteger_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.INTEGER, 12);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsIntegerAndValueIsNotInteger_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.INTEGER, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsIntegerArrayAndValueIsIntegerArray_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.INTEGER_ARRAY, new Integer[] { 5, 90 });

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsIntegerArrayAndValueIsNotIntegerArray_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.INTEGER_ARRAY, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsLongAndValueIsLong_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.LONG, 120L);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsLongAndValueIsNotLong_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.LONG, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsLongArrayAndValueIsLongArray_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.LONG_ARRAY, new Long[] { 500L, 900L });

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsLongArrayAndValueIsNotLongArray_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.LONG_ARRAY, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsNotManaged_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(99999, new String[] { "one", "two" });

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsNumberAndValueIsNotNumber_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.NUMBER, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsNumberAndValueIsNumber_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.NUMBER, 120);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsNumberArrayAndValueIsNotNumberArray_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.NUMBER_ARRAY, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsNumberArrayAndValueIsNumberArray_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.NUMBER_ARRAY, new Number[] { 500F, 900.63 });

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsShortAndValueIsNotShort_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.SHORT, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsShortAndValueIsShort_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.SHORT, 1);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsShortArrayAndValueIsNotShortArray_ThenReturnsFalse() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.SHORT_ARRAY, "invalidValue");

		assertThat(result, equalTo(false));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsShortArrayAndValueIsShortArray_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.SHORT_ARRAY, new Short[] { 1, 2 });

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsStringAndValueIsString_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.STRING, "myExpandoValue");

		assertThat(result, equalTo(true));
	}

	@Test
	public void isValueValidForColumn_WhenTypeIsStringArrayAndValueIsStringArray_ThenReturnsTrue() {
		boolean result = expandoValueValidationServiceImpl.isValueValidForColumn(ExpandoColumnConstants.STRING_ARRAY, new String[] { "myExpandoValue1", "myExpandoValue2" });

		assertThat(result, equalTo(true));
	}

}
