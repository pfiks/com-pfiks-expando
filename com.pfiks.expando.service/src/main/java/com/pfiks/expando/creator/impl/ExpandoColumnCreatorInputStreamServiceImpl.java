/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.creator.impl;

import java.io.InputStream;
import java.util.Optional;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.portal.kernel.model.Company;
import com.pfiks.expando.creator.ExpandoColumnCreatorInputStreamService;
import com.pfiks.expando.exception.ExpandoColumnCreationException;
import com.pfiks.expando.internal.model.ExpandoField;
import com.pfiks.expando.internal.service.ExpandoCreationService;

@Component(immediate = true, service = ExpandoColumnCreatorInputStreamService.class)
public class ExpandoColumnCreatorInputStreamServiceImpl implements ExpandoColumnCreatorInputStreamService {

	@Reference
	private ExpandoCreationService expandoCreationService;

	@Override
	public ExpandoColumn createExpandoColumn(Company company, String expandoColumnClassName, Class clazz, String inputStreamPath) throws ExpandoColumnCreationException {
		return createExpandoColumn(company, expandoColumnClassName, clazz.getClassLoader().getResourceAsStream(inputStreamPath));
	}

	@Override
	public ExpandoColumn createExpandoColumn(Company company, String expandoColumnClassName, InputStream inputStream) throws ExpandoColumnCreationException {
		ExpandoField expandoField = expandoCreationService.getExpandoFieldModelFromInputStream(inputStream);

		Optional<ExpandoColumn> existingExpandoColumn = expandoCreationService.getExistingExpandoColumn(company.getCompanyId(), expandoColumnClassName, expandoField);
		return existingExpandoColumn.isPresent() ? existingExpandoColumn.get() : expandoCreationService.createMissingExpandoColumn(company.getCompanyId(), expandoColumnClassName, expandoField);
	}

}
