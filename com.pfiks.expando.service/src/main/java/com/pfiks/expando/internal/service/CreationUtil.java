/**
 * Copyright (c) 2000-present Placecube Limited. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.expando.internal.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalService;
import com.liferay.expando.kernel.exception.NoSuchTableException;
import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.expando.kernel.model.ExpandoTable;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.expando.kernel.service.ExpandoTableLocalService;
import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.ResourcePermission;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ResourcePermissionLocalService;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.expando.internal.model.ExpandoField;
import com.pfiks.expando.internal.model.RolePermission;

@Component(immediate = true, service = CreationUtil.class)
public class CreationUtil {

	private static final Log LOG = LogFactoryUtil.getLog(CreationUtil.class);

	@Reference
	private CounterLocalService counterLocalService;

	@Reference
	private ExpandoColumnLocalService expandoColumnLocalService;

	@Reference
	private ExpandoTableLocalService expandoTableLocalService;

	@Reference
	private ResourcePermissionLocalService resourcePermissionLocalService;

	@Reference
	private RoleLocalService roleLocalService;

	public String getColumnTypeSettings(ExpandoField expandoField) {
		StringBuilder result = new StringBuilder();
		result.append(ExpandoColumnConstants.INDEX_TYPE + "=" + ExpandoColumnConstants.INDEX_TYPE_KEYWORD);
		result.append("\n");

		String hidden = expandoField.isHidden() ? "1" : "0";
		result.append(ExpandoColumnConstants.PROPERTY_HIDDEN + "=" + hidden);
		result.append("\n");

		String visibleWithUpdate = expandoField.isVisibleOnUpdate() ? "1" : "0";
		result.append(ExpandoColumnConstants.PROPERTY_VISIBLE_WITH_UPDATE_PERMISSION + "=" + visibleWithUpdate);
		result.append("\n");

		if (Validator.isNotNull(expandoField.getDisplayType())) {
			result.append(ExpandoColumnConstants.PROPERTY_DISPLAY_TYPE + "=" + expandoField.getDisplayType());
		}
		return result.toString();
	}

	public Object getDefaultValue(ExpandoField expandoField) {
		String defaultValue = expandoField.getDefaultValue();
		if (Validator.isNotNull(defaultValue)) {
			if (ExpandoColumnConstants.STRING == expandoField.getType()) {
				return defaultValue;
			} else if (ExpandoColumnConstants.STRING_ARRAY == expandoField.getType()) {
				return StringUtil.split(defaultValue, StringPool.COMMA);
			} else if (ExpandoColumnConstants.BOOLEAN == expandoField.getType()) {
				return Boolean.valueOf(defaultValue);
			}
		}
		return null;
	}

	public ExpandoTable getOrCreateDefaultTableForClass(long companyId, String className) throws PortalException {
		try {
			return expandoTableLocalService.getDefaultTable(companyId, className);
		} catch (NoSuchTableException e) {
			LOG.debug(e);
			LOG.info("No DefaultTable found for companyId: " + companyId + " and className: " + className + ". Creating new defaultTable.");
			return expandoTableLocalService.addDefaultTable(companyId, className);
		}
	}

	public Map<String, String[]> getRolePermissions(ExpandoField expandoField) {
		Map<String, String[]> rolesPermissions = new HashMap<>();

		List<RolePermission> rolePermissions = expandoField.getRolePermission();
		for (RolePermission rolePermissionModel : rolePermissions) {
			List<String> actionKeys = new ArrayList<>();
			if (rolePermissionModel.isUpdate()) {
				actionKeys.add(ActionKeys.UPDATE);
			}
			if (rolePermissionModel.isView()) {
				actionKeys.add(ActionKeys.VIEW);
			}
			if (!actionKeys.isEmpty()) {
				rolesPermissions.put(rolePermissionModel.getRoleName(), actionKeys.toArray(new String[actionKeys.size()]));
			}
		}
		return rolesPermissions;
	}

	public void configureColumnPermissionForRole(long companyId, long columnId, String roleName, String[] actionKeysValues) throws PortalException {
		if (Validator.isNotNull(roleName) && ArrayUtil.isNotEmpty(actionKeysValues)) {
			Role role = roleLocalService.getRole(companyId, roleName);

			String name = ExpandoColumn.class.getName();
			int scope = ResourceConstants.SCOPE_INDIVIDUAL;
			String primKey = String.valueOf(columnId);
			long roleId = role.getRoleId();
			ResourcePermission resourcePermission = getOrCreateResoucePermission(companyId, name, scope, primKey, roleId);
			resourcePermission.setActionIds(1);
			resourcePermission.setCompanyId(companyId);
			resourcePermission.setName(name);
			resourcePermission.setPrimKey(primKey);
			resourcePermission.setRoleId(roleId);
			resourcePermission.setScope(scope);
			resourcePermissionLocalService.updateResourcePermission(resourcePermission);
			resourcePermissionLocalService.setResourcePermissions(resourcePermission.getCompanyId(), resourcePermission.getName(), resourcePermission.getScope(), resourcePermission.getPrimKey(),
					resourcePermission.getRoleId(), actionKeysValues);
		}
	}

	public void setColumnPermissions(ExpandoColumn expandoColumn, Map<String, String[]> rolesPermissions) throws PortalException {
		if (null != rolesPermissions && !rolesPermissions.isEmpty()) {
			for (Entry<String, String[]> rolePermission : rolesPermissions.entrySet()) {
				configureColumnPermissionForRole(expandoColumn.getCompanyId(), expandoColumn.getColumnId(), rolePermission.getKey(), rolePermission.getValue());
			}
		}
	}

	public void setColumnSettings(ExpandoColumn expandoColumn, String columnSettings) throws PortalException {
		if (Validator.isNotNull(columnSettings)) {
			expandoColumnLocalService.updateTypeSettings(expandoColumn.getColumnId(), columnSettings);
		}
	}

	private ResourcePermission getOrCreateResoucePermission(long companyId, String name, int scope, String primKey, long roleId) {
		try {
			return resourcePermissionLocalService.getResourcePermission(companyId, name, scope, primKey, roleId);
		} catch (PortalException e) {
			LOG.debug(e);
			LOG.info("No ResourcePermission found for companyId: " + companyId + ", name: " + name + ", scope: " + scope + ", primKey: " + primKey + " and roleId: " + roleId);
			return resourcePermissionLocalService.createResourcePermission(counterLocalService.increment(ResourcePermission.class.getName()));
		}
	}

}
